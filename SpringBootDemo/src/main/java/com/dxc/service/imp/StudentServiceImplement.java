package com.dxc.service.imp;

import java.util.List;

import org.springframework.stereotype.Service;

import com.dxc.service.StudentService;
import com.dxc.entity.Student;
import com.dxc.repository.StudentRepository;

@Service
public class StudentServiceImplement implements StudentService{
	
	private StudentRepository studentRepository;
	
	public StudentServiceImplement(StudentRepository studentRepository) {
		super();
		this.studentRepository = studentRepository;
	}

	public List<Student> getAllStudents() {
		return studentRepository.findAll();
	}

	public Student saveStudent(Student student) {
		return studentRepository.save(student);
	}

	@Override
	public Student getStudentById(Integer id) {
		return studentRepository.findById(id).get();
	}

	public Student updateStudent(Student student) {
		return studentRepository.save(student);
	}

	public void deleteStudentById(Integer id) {
		studentRepository.deleteById(id);	
	}


}
